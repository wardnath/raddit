class Link < ActiveRecord::Base
	# app/models/link.rb
	acts_as_votable
	belongs_to :user
	has_many :comments
end
